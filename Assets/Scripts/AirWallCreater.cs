using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirWallCreater : MonoBehaviour
{
    public GameObject AirWall;

    void Start()
    {
        for (int i = -13; i <= 13; i += 2)
        {
            CreateItem(AirWall, new Vector3(i, -7), Quaternion.identity);
            CreateItem(AirWall, new Vector3(i, 7), Quaternion.identity);
        }
        for (int i = -5; i <= 5; i += 2)
        {
            CreateItem(AirWall, new Vector3(-13, i), Quaternion.identity);
            CreateItem(AirWall, new Vector3(13, i), Quaternion.identity);
        }
    }

    void Update()
    {

    }

    private void CreateItem(GameObject createCameObject, Vector3 createPosition, Quaternion createRotation)
    {
        GameObject item = Instantiate(createCameObject, createPosition, createRotation);
        item.transform.SetParent(gameObject.transform);
    }
}

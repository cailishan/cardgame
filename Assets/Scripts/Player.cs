using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //����
    public float moveSpeed;
    public float moveDistance;
    // ����
    public GameObject dashEffect;
    public Transform movePoint;
    public LayerMask whatStopsMovement;
    // Start is called before the first frame update
    void Start()
    {
        movePoint.SetParent(null);
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, movePoint.position, moveSpeed * Time.deltaTime);
        if (Vector3.Distance(transform.position, movePoint.position) <= .05f)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (!Physics2D.OverlapCircle(movePoint.position + Vector3.left * moveDistance, .2f, whatStopsMovement))
                {
                    Instantiate(dashEffect, transform.position, transform.rotation);
                    movePoint.position += Vector3.left * moveDistance;
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (!Physics2D.OverlapCircle(movePoint.position + Vector3.right * moveDistance, .2f, whatStopsMovement))
                {
                    Instantiate(dashEffect, transform.position, transform.rotation);
                    movePoint.position += Vector3.right * moveDistance;
                }

            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (!Physics2D.OverlapCircle(movePoint.position + Vector3.up * moveDistance, .2f, whatStopsMovement))
                {
                    Instantiate(dashEffect, transform.position, transform.rotation);
                    movePoint.position += Vector3.up * moveDistance;
                }
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (!Physics2D.OverlapCircle(movePoint.position + Vector3.down * moveDistance, .2f, whatStopsMovement))
                {
                    Instantiate(dashEffect, transform.position, transform.rotation);
                    movePoint.position += Vector3.down * moveDistance;
                }
            }
        }
    }
}
